//
//  CVWrapper.m
//  MobileOCR
//
//  Created by CodingMaster on 11/11/18.
//  Copyright © 2018 CodingMaster. All rights reserved.
//

#import "CVWrapper.h"
#import "UIImage+OpenCV.h"

using namespace std;
using namespace cv;

@implementation CVWrapper

+(UIImage*) getBinaryImg:(UIImage*) image
{
    cv::Mat input = [image CVMat];
    GaussianBlur(input, input, cv::Size(3,3), 0);
    resize(input, input, cv::Size(280, 35), 0, 0, INTER_CUBIC);
    cv::Mat gray,thresh;
    
    cvtColor(input, gray, cv::COLOR_BGR2GRAY);
    adaptiveThreshold(gray, thresh, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY_INV, 51, 7);
    
    UIImage *newImage = [UIImage imageWithCVMat:thresh];
    return newImage;
}

@end
