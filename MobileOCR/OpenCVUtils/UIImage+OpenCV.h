
#import <UIKit/UIKit.h>

@interface UIImage (OpenCV)

    //cv::Mat to UIImage
+ (UIImage *)imageWithCVMat:(const cv::Mat&)cvMat;
+ (UIImage *)imageWithCVMat:(const cv::Mat&)cvMat
                orientation:(UIImageOrientation)orientation;

- (id)initWithCVMat:(const cv::Mat&)cvMat
        orientation:(UIImageOrientation)orientation;

- (cv::Mat)CVMat;
- (cv::Mat)CVMat3;  
- (cv::Mat)CVGrayscaleMat;

@end
