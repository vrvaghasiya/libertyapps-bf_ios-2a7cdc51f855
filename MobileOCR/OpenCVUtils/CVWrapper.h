//
//  CVWrapper.h
//  MobileOCR
//
//  Created by CodingMaster on 11/11/18.
//  Copyright © 2018 CodingMaster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CVWrapper : NSObject

+ (UIImage*) getBinaryImg:(UIImage*) image;

@end

