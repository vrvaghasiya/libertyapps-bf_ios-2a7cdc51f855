//
//  AppDelegate.h
//  MobileOCR
//
//  Created by CodingMaster on 11/7/18.
//  Copyright © 2018 CodingMaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    bool isGrantedNotificationAccess;
}
@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;

@end

