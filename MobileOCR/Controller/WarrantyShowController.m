//
//  WarrantyShowController.m
//  MobileOCR
//
//  Created by CodingMaster on 1/31/19.
//  Copyright © 2019 CodingMaster. All rights reserved.
//

#import "WarrantyShowController.h"

@interface WarrantyShowController ()

@end

@implementation WarrantyShowController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UINavigationBar* navbar = self.navigationController.navigationBar;
    navbar.tintColor = [UIColor colorNamed:@"GrayTextColor"];
    self.title = @"Warranty";
    [navbar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorNamed:@"RedTextColor"]}];
    UIBarButtonItem *backBtn = [[UIBarButtonItem alloc]
                                initWithTitle:@"Back"
                                style:UIBarButtonItemStylePlain
                                target:self
                                action:@selector(backClick:)];
    self.navigationItem.leftBarButtonItem = backBtn;
}

-(IBAction)backClick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
