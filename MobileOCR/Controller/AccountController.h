//
//  AccountController.h
//  MobileOCR
//
//  Created by CodingMaster on 12/12/18.
//  Copyright © 2018 CodingMaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AccountController : UIViewController

@property NSString* email;
@property NSString* phone;
@property NSString* company;
@property NSString* person;

@end

