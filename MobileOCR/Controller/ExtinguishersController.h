//
//  ExtinguishersController.h
//  MobileOCR
//
//  Created by CodingMaster on 12/2/18.
//  Copyright © 2018 CodingMaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface ExtinguishersController : UIViewController

@property NSString* input_email;
@property NSString* input_phone;
@property NSString* input_companyName;
@property NSString* input_personName;
@property NSString* purchase_date;

@property (strong) NSMutableArray *dataArray;

@end


