//
//  WarrantyController.m
//  MobileOCR
//
//  Created by CodingMaster on 12/12/18.
//  Copyright © 2018 CodingMaster. All rights reserved.
//

#import "WarrantyController.h"
#import "ExtinguishersController.h"
#import "AppDelegate.h"
#import <FIRAuth.h>
#import <Firebase.h>

@interface WarrantyController ()
{
    UIAlertView *alert;
}

@end

@implementation WarrantyController

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if ([delegate respondsToSelector:@selector(persistentContainer)]) {
        context = delegate.persistentContainer.viewContext;
    }
    return context;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //navBar set
    UINavigationBar* navbar = self.navigationController.navigationBar;
    navbar.tintColor = [UIColor colorNamed:@"GrayTextColor"];
    self.title = @"Warranty";
    [navbar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorNamed:@"RedTextColor"]}];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]
                                  initWithTitle:@"Cancel"
                                  style:UIBarButtonItemStylePlain
                                  target:self
                                  action:@selector(cancelClick:)];
    self.navigationItem.rightBarButtonItem = cancelBtn;
    self.navigationItem.hidesBackButton = YES;
}

- (IBAction)acceptClick:(id)sender {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Create a new managed object
    NSManagedObject *newData = [NSEntityDescription insertNewObjectForEntityForName:@"Data" inManagedObjectContext:context];
    timestamp = [NSDate date].timeIntervalSince1970;
    [newData setValue:_email forKey:@"email"];
    [newData setValue:_dateString forKey:@"inspect_date"];
    [newData setValue:_location forKey:@"location"];
    [newData setValue:_result forKey:@"serial_num"];
    [newData setValue:[NSString stringWithFormat:@"%lli",timestamp] forKey:@"timestamp"];
    // Create a new managed object
    
    if ([self checkUserAvailable:_email] == false) {
        NSManagedObject *newUser = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
        [newUser setValue:_email forKey:@"email"];
        [newUser setValue:_phone forKey:@"phone"];
        [newUser setValue:_company forKey:@"company"];
        [newUser setValue:_person forKey:@"person"];
        [newUser setValue:_purchase_date forKey:@"purchase_date"];
        
        NSError *error = nil;
        // Save the object to persistent store
        if (![context save:&error]) {
            NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        }
    }
    
    [self addDataOnFireBase];
    
    NSString* message = @"The registration details for this device have been sent to Britannia Fire Ltd.";
    self->alert = [[UIAlertView alloc] initWithTitle:@"Done"
                                                  message:message
                                                 delegate:self
                                        cancelButtonTitle:@"OK"
                                        otherButtonTitles:nil];
    [self->alert show];
}
-(BOOL)checkUserAvailable :(NSString *)email{
    // Fetch the devices from persistent data store
     NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
     NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"User"];
     NSPredicate *predicate = [NSPredicate predicateWithFormat:@"email = %@",email];
     [fetchRequest setPredicate:predicate];
     NSMutableArray *array = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    if (array.count > 0) {
        return true;
    }else{
        return false;
    }
}

-(void)addDataOnFireBase{
    NSDictionary *post = @{@"email": _email,
                           @"inspect_date": _dateString,
                           @"location":_location,
                           @"serial_num": _result,
                           @"purchase_date": _purchase_date,
                          
    };
    
    self.ref = [[FIRDatabase database] reference];
    if ([FIRAuth auth].currentUser.uid != nil) {
        [[[[self.ref child:@"Data"] child:[FIRAuth auth].currentUser.uid] child:[NSString stringWithFormat:@"%lli",timestamp]] setValue:post];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [alert dismissWithClickedButtonIndex:-1 animated:YES];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ExtinguishersController *extinguishers = [storyboard instantiateViewControllerWithIdentifier:@"ExtinguishersController"];
        extinguishers.input_email = _email;
        extinguishers.input_phone = _phone;
        extinguishers.input_companyName = _company;
        extinguishers.input_personName = _person;
        extinguishers.purchase_date = _purchase_date;
        [self.navigationController pushViewController:extinguishers animated:YES];
    }
}

-(IBAction)cancelClick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
