//
//  ResultController.m
//  MobileOCR
//
//  Created by CodingMaster on 11/14/18.
//  Copyright © 2018 CodingMaster. All rights reserved.
//

#import "ResultController.h"

@interface ResultController ()
{
    UITextView* result;
    CALayer *rootLayer;
}

@end

@implementation ResultController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor grayColor];
    
    rootLayer = [[self view] layer];
    [rootLayer setMasksToBounds:YES];
    
    CGRect someRect = CGRectMake(0, 60, rootLayer.bounds.size.width, rootLayer.bounds.size.height);
    result = [[UITextView alloc] initWithFrame:someRect];
    [result setTextColor:[UIColor whiteColor]];
    [result setFont:[UIFont systemFontOfSize:15]];
    [result setScrollEnabled:YES];
    [result setBackgroundColor:[UIColor grayColor]];
    result.text = _resultstr;
    [self.view addSubview:result];
    
    //back button
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(returnClick:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Back" forState:UIControlStateNormal];
    button.frame = CGRectMake(rootLayer.bounds.size.width / 2 - 40, rootLayer.bounds.size.height - 40, 80, 40);
    [self.view addSubview:button];
}

-(IBAction)returnClick:(id)sender{
    ((ScanController*)self.tempVC).alertFlag = false;
    ((ScanController*)self.tempVC).result = @"";
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
