//
//  EmailAddressController.m
//  MobileOCR
//
//  Created by CodingMaster on 12/1/18.
//  Copyright © 2018 CodingMaster. All rights reserved.
//

#import "EmailAddressController.h"
#import "ExtinguishersController.h"
#import <Crashlytics/Crashlytics.h>
#import <Firebase.h>
#import <FIRAuth.h>
#import "AppDelegate.h"
#import <UserNotifications/UserNotifications.h>
#import <MBProgressHUD.h>
#import "Reachability.h"

@interface EmailAddressController () <UNUserNotificationCenterDelegate>
@property (weak, nonatomic) IBOutlet UITextField *passwordInput;
@property (weak, nonatomic) IBOutlet UITextField *emailInput;
@property (weak, nonatomic) IBOutlet UITextField *phoneInpute;
@property (weak, nonatomic) IBOutlet UITextField *companyNameInpute;
@property (weak, nonatomic) IBOutlet UITextField *personNameInpute;


@end

@implementation EmailAddressController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    Crashlytics.sharedInstance.debugMode = true;
        
    self.emailInput.delegate = self;
    _emailInput.returnKeyType = UIReturnKeyDone;
    _phoneInpute.returnKeyType = UIReturnKeyDone;
    _companyNameInpute.returnKeyType = UIReturnKeyDone;
    _personNameInpute.returnKeyType = UIReturnKeyDone;
    [_emailInput setKeyboardType:UIKeyboardTypeEmailAddress];
    
    UINavigationBar* navbar = self.navigationController.navigationBar;
    self.title = @"Account";
    navbar.tintColor = [UIColor colorNamed:@"GrayTextColor"];
    [navbar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorNamed:@"RedTextColor"]}];
    
//    self.navigationItem.hidesBackButton = YES;
}
- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = false;
    
    handeler = [[FIRAuth auth]
    addAuthStateDidChangeListener:^(FIRAuth *_Nonnull auth, FIRUser *_Nullable user) {
    }];
}
- (void)viewWillDisappear:(BOOL)animated{
    [[FIRAuth auth] removeAuthStateDidChangeListener:handeler];
}

- (IBAction)submitClick:(id)sender {
    if (_emailInput.text.length > 0 && _passwordInput.text.length > 0 && _phoneInpute.text.length > 0 && _companyNameInpute.text.length > 0 && _personNameInpute.text > 0) {
        
        strEmail=_emailInput.text.lowercaseString;
        strPhone = _phoneInpute.text;
        strPersonalName = _personNameInpute.text;
        strCompanyName = _companyNameInpute.text;

        Reachability* reachability = [Reachability reachabilityWithHostName:@"www.apple.com"];
        NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];

        if (remoteHostStatus == NotReachable) {
            NSLog(@"not reachable");
//            [self MoveNextSceen];
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"No internet!" message:@"Internet connection is required." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action_OK = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertVC addAction:action_OK];
            [self presentViewController:alertVC animated:true completion:nil];
        }else{
            [MBProgressHUD showHUDAddedTo:self.view animated:true];
            [self CreateUserOnFirebase];
        }
    }
    else{
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"All field is Required!" message:@"Please enter a valid field value." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action_OK = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertVC addAction:action_OK];
        [self presentViewController:alertVC animated:true completion:nil];
    }
}
-(void)signInFirebase{
    [[FIRAuth auth] signInWithEmail:self->strEmail
                           password:self.passwordInput.text
                         completion:^(FIRAuthDataResult * _Nullable authResult,
                                      NSError * _Nullable error) {
        if (error == nil) {
            NSLog(@"userid %@",authResult.user.uid);
            [self syncDB];
            [self getDataFromFirebase];
            [self AddUserDetails:authResult.user.uid :self->strEmail :self->strPhone :self->strCompanyName :self->strPersonalName];
            [self MoveNextSceen];
        }else{
            [MBProgressHUD hideHUDForView:self.view animated:true];
            NSLog(@"error :%@",error.description);
           UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"Login Failed" message:@"Email or password is wrong.." preferredStyle:UIAlertControllerStyleAlert];
           UIAlertAction *action_OK = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
           [alertVC addAction:action_OK];
           [self presentViewController:alertVC animated:true completion:nil];
        }
    }];
}
-(void)CreateUserOnFirebase{
    [[FIRAuth auth] createUserWithEmail:self->strEmail password:self.passwordInput.text completion:^(FIRAuthDataResult * _Nullable authResult, NSError * _Nullable error) {
        NSLog(@"userid %@",authResult.user.uid);
        if (error == nil) {
            [self syncDB];
            [self AddUserDetails:authResult.user.uid :self->strEmail :self->strPhone :self->strCompanyName :self->strPersonalName];
            [self MoveNextSceen];
        }else{
//            [self signInFirebase];
            [MBProgressHUD hideHUDForView:self.view animated:true];
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"Register failed" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
                      UIAlertAction *action_OK = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                      [alertVC addAction:action_OK];
                      [self presentViewController:alertVC animated:true completion:nil];
        }
    }];
}
-(void)MoveNextSceen{
    [MBProgressHUD hideHUDForView:self.view animated:true];
    NSDate * now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd-MM-yyyy"];
    NSString *newDateString = [outputFormatter stringFromDate:now];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ExtinguishersController *extinguishers = [storyboard instantiateViewControllerWithIdentifier:@"ExtinguishersController"];
    extinguishers.input_email = _emailInput.text.lowercaseString;
    extinguishers.input_phone = _phoneInpute.text;
    extinguishers.input_companyName = _companyNameInpute.text;
    extinguishers.input_personName = _personNameInpute.text;
    extinguishers.purchase_date = newDateString;
    [self.navigationController pushViewController:extinguishers animated:YES];
}

-(void)syncDB{
    // Fetch the devices from persistent data store
       NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
       NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Data"];
       NSPredicate *predicate = [NSPredicate predicateWithFormat:@"email = %@",strEmail];
       [fetchRequest setPredicate:predicate];
       self.dataArray = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        int OldRecord = 0 ;
        for (NSManagedObject *data in self.dataArray) {
            NSDictionary *post = @{@"email": [data valueForKey:@"email"],
                           @"inspect_date": [data valueForKey:@"inspect_date"],
                           @"location":[data valueForKey:@"location"],
                           @"serial_num": [data valueForKey:@"serial_num"],
                           @"purchase_date": @"31-10-2020",
            };
            long long timestamp = [[data valueForKey:@"timestamp"] longLongValue];
            if (timestamp > 0 ) {
                [self addDataOnFireBase:post :timestamp];
            }else{
                long long current_timestamp = [NSDate date].timeIntervalSince1970;
                [self addDataOnFireBase:post :current_timestamp + OldRecord];
                [self updateTimeStamp:post :[NSString stringWithFormat:@"%lli",current_timestamp + OldRecord]];
                OldRecord += 1;
            }
        }
    
//    [self getDataFromFirebase];
}
-(void)AddUserDetails: (NSString *)userId :(NSString *)email :(NSString *)phone :(NSString *)companyName :(NSString *)personName{
    NSDictionary *post = @{@"email": email,
                   @"phone": phone,
                   @"companyName":companyName,
                   @"personName":personName
    };
    
    self.ref = [[FIRDatabase database] reference];
    if ([FIRAuth auth].currentUser.uid != nil) {
        [[[self.ref child:@"User"] child:userId] setValue:post];
    }
}
-(void)addDataOnFireBase:(NSDictionary *)post :(long long)timestamp {
    self.ref = [[FIRDatabase database] reference];
    if ([FIRAuth auth].currentUser.uid != nil) {
        [[[[self.ref child:@"Data"] child:[FIRAuth auth].currentUser.uid] child:[NSString stringWithFormat:@"%lli",timestamp]] setValue:post];
    }
}
-(void)getDataFromFirebase{
    self.ref = [[FIRDatabase database] reference];
    [[[self.ref child:@"Data"] child:[FIRAuth auth].currentUser.uid] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if(snapshot.value != [NSNull null]) {
            // Yes! there is a record.
                NSDictionary *postDict = snapshot.value;
            NSLog(@"post dic %lu",(unsigned long)postDict.count);
                    
                    for (NSString *key in postDict.allKeys) {
                        NSDictionary *dic = [postDict valueForKey:key];
                        [self checkDataAvailable:dic : key];
                    }
        }
    }];
}
-(void)updateTimeStamp:(NSDictionary *)dic  :(NSString *)timestamp{
    // Fetch the devices from persistent data store
    NSString *email = [dic valueForKey:@"email"];
    NSString *inspect_date = [dic valueForKey:@"inspect_date"];
    NSString *serial_num = [dic valueForKey:@"serial_num"];
     NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
     NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Data"];
     NSPredicate *predicate = [NSPredicate predicateWithFormat:@"email = %@ && inspect_date = %@ && serial_num = %@",email,inspect_date,serial_num];
     [fetchRequest setPredicate:predicate];
    NSMutableArray *result = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    if (result.count > 0) {
        NSManagedObject* favoritsGrabbed = [result objectAtIndex:0];
        [favoritsGrabbed setValue:email forKey:@"email"];
        [favoritsGrabbed setValue:[dic valueForKey:@"inspect_date"] forKey:@"inspect_date"];
        [favoritsGrabbed setValue:[dic valueForKey:@"location"] forKey:@"location"];
        [favoritsGrabbed setValue:[dic valueForKey:@"serial_num"] forKey:@"serial_num"];
        [favoritsGrabbed setValue:timestamp forKey:@"timestamp"];
    }
    NSError *error = nil;
       // Save the object to persistent store
    if (![managedObjectContext save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
}
-(void)checkDataAvailable :(NSDictionary *)dic  :(NSString *)timestamp{
    // Fetch the devices from persistent data store
    NSString *email = [dic valueForKey:@"email"];
     NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
     NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Data"];
     NSPredicate *predicate = [NSPredicate predicateWithFormat:@"email = %@ && timestamp = %@",email,timestamp];
     [fetchRequest setPredicate:predicate];
    NSMutableArray *result = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    if (result.count > 0) {
        NSManagedObject* favoritsGrabbed = [result objectAtIndex:0];
        [favoritsGrabbed setValue:email forKey:@"email"];
        [favoritsGrabbed setValue:[dic valueForKey:@"inspect_date"] forKey:@"inspect_date"];
        [favoritsGrabbed setValue:[dic valueForKey:@"location"] forKey:@"location"];
        [favoritsGrabbed setValue:[dic valueForKey:@"serial_num"] forKey:@"serial_num"];
        [favoritsGrabbed setValue:timestamp forKey:@"timestamp"];

    }else{
        NSManagedObject *newData = [NSEntityDescription insertNewObjectForEntityForName:@"Data" inManagedObjectContext:managedObjectContext];
        [newData setValue:email forKey:@"email"];
           [newData setValue:[dic valueForKey:@"inspect_date"] forKey:@"inspect_date"];
           [newData setValue:[dic valueForKey:@"location"] forKey:@"location"];
           [newData setValue:[dic valueForKey:@"serial_num"] forKey:@"serial_num"];
           [newData setValue:timestamp forKey:@"timestamp"];
    }
    NSError *error = nil;
    // Save the object to persistent store
    if (![managedObjectContext save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    [[NSNotificationCenter defaultCenter]
          postNotificationName:@"ReloadData"
          object:self];
}

#pragma mark - Hide textfield...

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y +=animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect textFieldRect=[self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect=[self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline=textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - 0.2 * viewRect.size.height;
    CGFloat denominator = (0.8 - 0.2) * viewRect.size.height;
    CGFloat heightFraction=numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction= 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication]statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floorf(216 * heightFraction);
    }
    else
    {
        animatedDistance = floorf(140 * heightFraction);
    }
    CGRect viewFrame=self.view.frame;
    viewFrame.origin.y-=animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if ([delegate respondsToSelector:@selector(persistentContainer)]) {
        context = delegate.persistentContainer.viewContext;
    }
    return context;
}

@end
