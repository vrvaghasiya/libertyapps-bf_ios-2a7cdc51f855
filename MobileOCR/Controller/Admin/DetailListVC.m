//
//  DetailListVC.m
//  MobileOCR
//
//  Created by Vishal on 05/11/20.
//  Copyright © 2020 CodingMaster. All rights reserved.
//

#import "DetailListVC.h"
#import <MBProgressHUD.h>
#import "ExtinguisherDetailController.h"


@interface DetailListVC ()

@end

@implementation DetailListVC
@synthesize userId;
- (void)viewDidLoad {
    [super viewDidLoad];
    UINavigationBar* navbar = self.navigationController.navigationBar;
     navbar.tintColor = [UIColor colorNamed:@"GrayTextColor"];
     self.title = @"Extinguishers";
     [navbar setTitleTextAttributes:
      @{NSForegroundColorAttributeName:[UIColor colorNamed:@"RedTextColor"]}];
    [self performSelector:@selector(loadUser) withObject:nil afterDelay:0.5];

}
-(void)loadUser{
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    self.ref = [[FIRDatabase database] reference];
      [[[self.ref child:@"Data"] child:userId] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
          if(snapshot.value != [NSNull null]) {
              // Yes! there is a record.
              self->_dataDic = snapshot.value;
              [self->_tbl reloadData];
          }
          [MBProgressHUD hideHUDForView:self.view animated:true];
      }];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataDic.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat selfHeight=self.view.bounds.size.height / 10;
    return selfHeight;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    
    UITableViewCell *cell = [self.tbl dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    NSDictionary *data = [_dataDic valueForKey:[_dataDic.allKeys objectAtIndex:indexPath.row]];
    
    CGFloat wid = cell.bounds.size.width;
    CGFloat selfHeight = self.view.bounds.size.height / 9;
    //Initialize Label
    UILabel *lbl1 = [[UILabel alloc]initWithFrame:CGRectMake(20,
                                                             0,
                                                             wid - 20,
                                                             selfHeight / 2)];
    [lbl1 setFont:[UIFont systemFontOfSize:20.0]];
    [lbl1 setTextColor:[UIColor colorNamed:@"DarkGrayTextColor"]];
    lbl1.text = [data valueForKey:@"serial_num"];
    [cell addSubview:lbl1];
    
    UILabel *lbl2 = [[UILabel alloc]initWithFrame:CGRectMake(20 ,
                                                             selfHeight / 2,
                                                             wid - 20,
                                                             selfHeight / 2)];
    [lbl2 setFont:[UIFont systemFontOfSize:14.0]];
    [lbl2 setTextColor:[UIColor colorNamed:@"GrayTextColor"]];
    lbl2.text = [@"Last Inspected: " stringByAppendingString:[data valueForKey:@"inspect_date"]];
    [cell addSubview:lbl2];
     
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSDictionary *data = [_dataDic valueForKey:[_dataDic.allKeys objectAtIndex:indexPath.row]];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ExtinguisherDetailController *extinguishers = [storyboard instantiateViewControllerWithIdentifier:@"ExtinguisherDetailController"];
    extinguishers.purchase_date = [data valueForKey:@"inspect_date"];
    extinguishers.phone = _phone;
    extinguishers.company = _company;
    extinguishers.person = _person;
    extinguishers.dataDic = data;
    extinguishers.isAdmin = true;
    [self.navigationController pushViewController:extinguishers animated:YES];
}


@end
