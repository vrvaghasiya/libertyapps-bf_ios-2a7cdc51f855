//
//  UserlistVC.m
//  MobileOCR
//
//  Created by Vishal on 05/11/20.
//  Copyright © 2020 CodingMaster. All rights reserved.
//

#import "UserlistVC.h"
#import <Firebase.h>
#import <FIRAuth.h>
#import <MBProgressHUD.h>
#import "UserCell.h"
#import "DetailListVC.h"
@interface UserlistVC ()

@end

@implementation UserlistVC
@synthesize userId;
- (void)viewDidLoad {
    [super viewDidLoad];
    UINavigationBar* navbar = self.navigationController.navigationBar;
    self.title = @"User List";
    navbar.tintColor = [UIColor colorNamed:@"GrayTextColor"];
    [navbar setTitleTextAttributes:
        @{NSForegroundColorAttributeName:[UIColor colorNamed:@"RedTextColor"]}];
    self.navigationItem.hidesBackButton = YES;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(Logout)];

    self.navigationController.navigationBarHidden = false;
    [self performSelector:@selector(loadUser) withObject:nil afterDelay:0.5];
}
-(void)loadUser{
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    self.ref = [[FIRDatabase database] reference];
      [[self.ref child:@"User"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
          if(snapshot.value != [NSNull null]) {
              // Yes! there is a record.
              self->_dataDic = snapshot.value;
              
              [self->_tbl reloadData];
          }
          [MBProgressHUD hideHUDForView:self.view animated:true];
      }];
}
-(void)Logout{
    [self.navigationController popViewControllerAnimated:true];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self->_dataDic.allKeys.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserCell *cell = (UserCell *)[_tbl dequeueReusableCellWithIdentifier:@"Cell"];
    NSDictionary *dic = [_dataDic valueForKey:[_dataDic.allKeys objectAtIndex:indexPath.row]];
    cell.lblEmail.text = [dic valueForKey:@"email"];
    cell.lblPersonName.text = [dic valueForKey:@"personName"];
    cell.lblPhone.text = [dic valueForKey:@"phone"];
    cell.lblCompanyName.text = [dic valueForKey:@"companyName"];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSDictionary *dic = [_dataDic valueForKey:[_dataDic.allKeys objectAtIndex:indexPath.row]];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DetailListVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DetailListVC"];
    vc.userId = [_dataDic.allKeys objectAtIndex:indexPath.row];
    vc.company = [dic valueForKey:@"companyName"];
    vc.person = [dic valueForKey:@"personName"];
    vc.phone = [dic valueForKey:@"personName"];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
