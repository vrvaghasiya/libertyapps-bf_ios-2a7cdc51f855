//
//  AdminLoginVC.h
//  MobileOCR
//
//  Created by Vishal on 05/11/20.
//  Copyright © 2020 CodingMaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <FirebaseAuth.h>
NS_ASSUME_NONNULL_BEGIN

@interface AdminLoginVC : UIViewController
{
    FIRAuthStateDidChangeListenerHandle handeler;
    NSString *email;
    NSString *personName;
    NSString *phoneNumner;
    NSString *companyName;
    float animatedDistance;
}
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (strong, nonatomic) IBOutlet UIView *viewResetPassword;
@end
NS_ASSUME_NONNULL_END
