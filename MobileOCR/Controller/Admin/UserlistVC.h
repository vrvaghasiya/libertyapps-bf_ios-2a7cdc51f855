//
//  UserlistVC.h
//  MobileOCR
//
//  Created by Vishal on 05/11/20.
//  Copyright © 2020 CodingMaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <FirebaseAuth.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserlistVC : UIViewController
{
    
}
@property (weak, nonatomic) IBOutlet UITableView *tbl;
@property(nonatomic,retain)NSString *userId;
@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (strong,nonatomic)  NSDictionary *dataDic;

@end

NS_ASSUME_NONNULL_END
