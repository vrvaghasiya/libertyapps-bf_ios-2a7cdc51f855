//
//  AdminLoginVC.m
//  MobileOCR
//
//  Created by Vishal on 05/11/20.
//  Copyright © 2020 CodingMaster. All rights reserved.
//

#import "AdminLoginVC.h"
#import "ExtinguishersController.h"
#import <Firebase.h>
#import <FIRAuth.h>
#import <MBProgressHUD.h>
#import "UserlistVC.h"
#import <Crashlytics/Crashlytics.h>
#import "AppDelegate.h"
#import <MBProgressHUD.h>
#import "Reachability.h"
@interface AdminLoginVC ()

@end

@implementation AdminLoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
}
- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    handeler = [[FIRAuth auth]
    addAuthStateDidChangeListener:^(FIRAuth *_Nonnull auth, FIRUser *_Nullable user) {
    }];
}
- (void)viewWillDisappear:(BOOL)animated{
    [[FIRAuth auth] removeAuthStateDidChangeListener:handeler];
}
- (IBAction)btnLogin_click:(id)sender {
    if (_txtEmail.text.length > 0 && _txtPassword.text.length > 0) {
        
        
        Reachability* reachability = [Reachability reachabilityWithHostName:@"www.apple.com"];
        NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
        if (remoteHostStatus == NotReachable) {
            
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"No internet!" message:@"Internet connection is required." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action_OK = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertVC addAction:action_OK];
            [self presentViewController:alertVC animated:true completion:nil];
            return;
        }
        
        [MBProgressHUD showHUDAddedTo:self.view animated:true];
        
        [[FIRAuth auth] signInWithEmail:_txtEmail.text
                                 password:_txtPassword.text
                               completion:^(FIRAuthDataResult * _Nullable authResult,
                                            NSError * _Nullable error) {
              if (error == nil) {
                  NSLog(@"userid %@",authResult.user.uid);
                  
                  
                  self.ref = [[FIRDatabase database] reference];
                  
                 [[self.ref child:@"Admin"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                     if(snapshot.value != [NSNull null]) {
                         // Yes! there is a record.
                         NSString *strAdminId = [snapshot.value valueForKey:@"id"];
                         if ([strAdminId isEqualToString:authResult.user.uid]) {
                             NSLog(@"Login Success..");
                             [MBProgressHUD hideHUDForView:self.view animated:true];
                             self->_txtEmail.text = @"";
                             self->_txtPassword.text = @"";
                             [self->_txtEmail resignFirstResponder];
                             [self->_txtPassword resignFirstResponder];
                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            UserlistVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"UserlistVC"];
                            [self.navigationController pushViewController:vc animated:YES];
                             
                         }else{
                            NSLog(@"userid %@",authResult.user.uid);
                            [self syncDB];
                            [self getDataFromFirebase];
                         }
                     }
                 }];
              }else{
                  [MBProgressHUD hideHUDForView:self.view animated:true];
                  NSLog(@"error :%@",error.description);
                  UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"Login Failed" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
                  UIAlertAction *action_OK = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                  [alertVC addAction:action_OK];
                  [self presentViewController:alertVC animated:true completion:nil];
              }
          }];
        
    }else{
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"All field is Required!" message:@"Please enter a valid field value." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action_OK = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertVC addAction:action_OK];
        [self presentViewController:alertVC animated:true completion:nil];
    }
}
- (IBAction)btnForgotPassword_click:(id)sender {
    
    UIAlertController *forgotPasswordAlert = [UIAlertController alertControllerWithTitle:@"Forgot password?" message:@"Enter email address" preferredStyle:UIAlertControllerStyleAlert];
    [forgotPasswordAlert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Enter email address";
    }];
    UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *btnReset = [UIAlertAction actionWithTitle:@"Reset Password" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *resetEmail = forgotPasswordAlert.textFields.firstObject.text;
        NSLog(@"Email : %@",resetEmail);
        [[FIRAuth auth] sendPasswordResetWithEmail:resetEmail completion:^(NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (error == nil) {
                    UIAlertController *resetEmailSentAlert = [UIAlertController alertControllerWithTitle:@"Reset email sent successfully" message:@"Check your email" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *btnOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                    [resetEmailSentAlert addAction:btnOk];
                    [self presentViewController:resetEmailSentAlert animated:YES completion:nil];
                }else{
                    UIAlertController *resetFailedAlert = [UIAlertController alertControllerWithTitle:@"Reset Failed" message:[NSString stringWithFormat:@"Error: %@",error.localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *btnOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                    [resetFailedAlert addAction:btnOk];
                    [self presentViewController:resetFailedAlert animated:YES completion:nil];
                }
            });
        }];
    }];
    [forgotPasswordAlert addAction:btnCancel];
    [forgotPasswordAlert addAction:btnReset];
    [self presentViewController:forgotPasswordAlert animated:YES completion:nil];
}
-(void)MoveNextSceen{
    [MBProgressHUD hideHUDForView:self.view animated:true];
    [_txtEmail resignFirstResponder];
    [_txtPassword resignFirstResponder];
    _txtEmail.text = @"";
    _txtPassword.text = @"";
    NSDate * now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd-MM-yyyy"];
    NSString *newDateString = [outputFormatter stringFromDate:now];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ExtinguishersController *extinguishers = [storyboard instantiateViewControllerWithIdentifier:@"ExtinguishersController"];
    extinguishers.input_email = email.lowercaseString;
    extinguishers.input_phone = phoneNumner;
    extinguishers.input_companyName = companyName;
    extinguishers.input_personName = personName;
    extinguishers.purchase_date = newDateString;
    [self.navigationController pushViewController:extinguishers animated:YES];
}

-(void)syncDB{
    // Fetch the devices from persistent data store
       NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
       NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Data"];
       NSPredicate *predicate = [NSPredicate predicateWithFormat:@"email = %@",email.lowercaseString];
       [fetchRequest setPredicate:predicate];
       NSMutableArray *dataArray = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        int OldRecord = 0 ;
        for (NSManagedObject *data in dataArray) {
            NSDictionary *post = @{@"email": [data valueForKey:@"email"],
                           @"inspect_date": [data valueForKey:@"inspect_date"],
                           @"location":[data valueForKey:@"location"],
                           @"serial_num": [data valueForKey:@"serial_num"],
                           @"purchase_date": @"31-10-2020",
            };
            long long timestamp = [[data valueForKey:@"timestamp"] longLongValue];
            if (timestamp > 0 ) {
                [self addDataOnFireBase:post :timestamp];
            }else{
                long long current_timestamp = [NSDate date].timeIntervalSince1970;
                [self addDataOnFireBase:post :current_timestamp + OldRecord];
                [self updateTimeStamp:post :[NSString stringWithFormat:@"%lli",current_timestamp + OldRecord]];
                OldRecord += 1;
            }
        }
    
//    [self getDataFromFirebase];
}
-(void)addDataOnFireBase:(NSDictionary *)post :(long long)timestamp {
    self.ref = [[FIRDatabase database] reference];
    if ([FIRAuth auth].currentUser.uid != nil) {
        [[[[self.ref child:@"Data"] child:[FIRAuth auth].currentUser.uid] child:[NSString stringWithFormat:@"%lli",timestamp]] setValue:post];
    }
}
-(void)getDataFromFirebase{
    self.ref = [[FIRDatabase database] reference];
    
    [[[self.ref child:@"User"] child:[FIRAuth auth].currentUser.uid] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if(snapshot.value != [NSNull null]) {
            // Yes! there is a record.
                NSDictionary *userDict = snapshot.value;
            NSLog(@"User Details dic %lu",(unsigned long)userDict.count);
            self->email = self.txtEmail.text.lowercaseString;
            self->personName = [userDict valueForKey:@"personName"];
            self->companyName = [userDict valueForKey:@"companyName"];
            self->phoneNumner = [userDict valueForKey:@"phone"];
             [self MoveNextSceen];
            [MBProgressHUD hideHUDForView:self.view animated:true];
        }else{
            [MBProgressHUD hideHUDForView:self.view animated:true];
        }
    }];
    
    [[[self.ref child:@"Data"] child:[FIRAuth auth].currentUser.uid] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if(snapshot.value != [NSNull null]) {
            // Yes! there is a record.
                NSDictionary *postDict = snapshot.value;
            NSLog(@"post dic %lu",(unsigned long)postDict.count);
                    
                    for (NSString *key in postDict.allKeys) {
                        NSDictionary *dic = [postDict valueForKey:key];
                        [self checkDataAvailable:dic : key];
                    }
        }
    }];
}
-(void)updateTimeStamp:(NSDictionary *)dic  :(NSString *)timestamp{
    // Fetch the devices from persistent data store
    NSString *email = [dic valueForKey:@"email"];
    NSString *inspect_date = [dic valueForKey:@"inspect_date"];
    NSString *serial_num = [dic valueForKey:@"serial_num"];
     NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
     NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Data"];
     NSPredicate *predicate = [NSPredicate predicateWithFormat:@"email = %@ && inspect_date = %@ && serial_num = %@",email,inspect_date,serial_num];
     [fetchRequest setPredicate:predicate];
    NSMutableArray *result = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    if (result.count > 0) {
        NSManagedObject* favoritsGrabbed = [result objectAtIndex:0];
        [favoritsGrabbed setValue:email forKey:@"email"];
        [favoritsGrabbed setValue:[dic valueForKey:@"inspect_date"] forKey:@"inspect_date"];
        [favoritsGrabbed setValue:[dic valueForKey:@"location"] forKey:@"location"];
        [favoritsGrabbed setValue:[dic valueForKey:@"serial_num"] forKey:@"serial_num"];
        [favoritsGrabbed setValue:timestamp forKey:@"timestamp"];
    }
    NSError *error = nil;
       // Save the object to persistent store
    if (![managedObjectContext save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
}
-(void)checkDataAvailable :(NSDictionary *)dic  :(NSString *)timestamp{
    // Fetch the devices from persistent data store
    NSString *email = [dic valueForKey:@"email"];
     NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
     NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Data"];
     NSPredicate *predicate = [NSPredicate predicateWithFormat:@"email = %@ && timestamp = %@",email,timestamp];
     [fetchRequest setPredicate:predicate];
    NSMutableArray *result = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    if (result.count > 0) {
        NSManagedObject* favoritsGrabbed = [result objectAtIndex:0];
        [favoritsGrabbed setValue:email forKey:@"email"];
        [favoritsGrabbed setValue:[dic valueForKey:@"inspect_date"] forKey:@"inspect_date"];
        [favoritsGrabbed setValue:[dic valueForKey:@"location"] forKey:@"location"];
        [favoritsGrabbed setValue:[dic valueForKey:@"serial_num"] forKey:@"serial_num"];
        [favoritsGrabbed setValue:timestamp forKey:@"timestamp"];

    }else{
        NSManagedObject *newData = [NSEntityDescription insertNewObjectForEntityForName:@"Data" inManagedObjectContext:managedObjectContext];
        [newData setValue:email forKey:@"email"];
           [newData setValue:[dic valueForKey:@"inspect_date"] forKey:@"inspect_date"];
           [newData setValue:[dic valueForKey:@"location"] forKey:@"location"];
           [newData setValue:[dic valueForKey:@"serial_num"] forKey:@"serial_num"];
           [newData setValue:timestamp forKey:@"timestamp"];
    }
    NSError *error = nil;
    // Save the object to persistent store
    if (![managedObjectContext save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    [[NSNotificationCenter defaultCenter]
          postNotificationName:@"ReloadData"
          object:self];
}
- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if ([delegate respondsToSelector:@selector(persistentContainer)]) {
        context = delegate.persistentContainer.viewContext;
    }
    return context;
}
#pragma mark - Hide textfield...

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect textFieldRect=[self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect=[self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline=textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - 0.2 * viewRect.size.height;
    CGFloat denominator = (0.8 - 0.2) * viewRect.size.height;
    CGFloat heightFraction=numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction= 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication]statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floorf(216 * heightFraction);
    }
    else
    {
        animatedDistance = floorf(140 * heightFraction);
    }
    CGRect viewFrame=self.view.frame;
    viewFrame.origin.y-=animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
@end
