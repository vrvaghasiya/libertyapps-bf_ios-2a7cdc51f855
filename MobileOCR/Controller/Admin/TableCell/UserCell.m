//
//  UserCell.m
//  MobileOCR
//
//  Created by Vishal on 05/11/20.
//  Copyright © 2020 CodingMaster. All rights reserved.
//

#import "UserCell.h"

@implementation UserCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
