//
//  UserCell.h
//  MobileOCR
//
//  Created by Vishal on 05/11/20.
//  Copyright © 2020 CodingMaster. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblPersonName;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblCompanyName;

@end

NS_ASSUME_NONNULL_END
