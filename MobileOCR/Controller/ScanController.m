//
//  ViewController.m
//  MobileOCR
//
//  Created by CodingMaster on 11/7/18.
//  Copyright © 2018 CodingMaster. All rights reserved.
//

#import "AppDelegate.h"
#import "ScanController.h"
#import "RegisterController.h"
#import "ExtinguishersController.h"
#import "ExtinguisherDetailController.h"
#import "RegisterController.h"

@interface ScanController ()
{
    //camera set
    AVCaptureSession *session;
}

@end

@implementation ScanController

- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if ([delegate respondsToSelector:@selector(persistentContainer)]) {
        context = delegate.persistentContainer.viewContext;
    }
    
    return context;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    // Fetch the devices from persistent data store
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Data"];
    NSMutableArray* fetchArray = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    self.dataArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < fetchArray.count; i++){
        NSManagedObject *data = [fetchArray objectAtIndex:i];
        NSString* emailStr = [data valueForKey:@"email"];
        
        if ([_email isEqualToString:emailStr])
        {
            [self.dataArray addObject:data];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //navBar set
    UINavigationBar* navbar = self.navigationController.navigationBar;
    navbar.tintColor = [UIColor colorNamed:@"GrayTextColor"];
    self.title = @"Scan serial number";
    [navbar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorNamed:@"RedTextColor"]}];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]
                                initWithTitle:@"Cancel"
                                style:UIBarButtonItemStylePlain
                                target:self
                                action:@selector(cancelClick:)];
    self.navigationItem.rightBarButtonItem = cancelBtn;
    self.navigationItem.hidesBackButton = YES;
    
    session = [[AVCaptureSession alloc] init];
    [session setSessionPreset:AVCaptureSessionPresetPhoto];
    AVCaptureDevice *inputDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error;
    AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:inputDevice error:&error];
    
    if ([session canAddInput:deviceInput]) {
        [session addInput:deviceInput];
    }
    
    AVCaptureVideoPreviewLayer *previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
    [previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    CALayer *rootLayer = [[self view] layer];
    [rootLayer setMasksToBounds:YES];
    int frameWid = rootLayer.bounds.size.width / 7 * 5;
    int frameHei = frameWid;
    int offset = rootLayer.bounds.size.height / 10;
    
    UILabel *notification = [[UILabel alloc]initWithFrame:CGRectMake(0, frameWid / 5 + offset, rootLayer.bounds.size.width, 15)];
    notification.text = @"Position the serial number between the yellow lines";
    notification.textColor = [UIColor colorNamed:@"GrayTextColor"];
    notification.textAlignment = NSTextAlignmentCenter;
    [notification setFont: [notification.font fontWithSize: 12]];
    [self.view addSubview:notification];
    
    CGRect frame = CGRectMake(frameWid / 5, frameWid / 5 + offset + 30 , frameWid, frameWid);
    
    [previewLayer setFrame:frame];
    previewLayer.borderColor = [UIColor colorNamed:@"DarkerGrayTextColor"].CGColor;
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        previewLayer.borderWidth = 13.0;
    }
    else{
        previewLayer.borderWidth = 6.0;
    }
    
    [rootLayer insertSublayer:previewLayer atIndex:0];
    
    float ltx = frameWid / 5 + frameWid/6;
    float lty = frameWid / 5 + offset + 30 + frameHei/7*3;
    float lbx = ltx;
    float lby = frameWid / 5 + offset + 30 + frameHei/7*4;
    float rtx = frameWid*5 / 6 + frameWid / 5;
    float rty = lty;
    float rbx = rtx;
    float rby = lby;
    
    [self makeLineLayer:rootLayer lineFromPointA:CGPointMake(ltx, lty) toPointB:CGPointMake(rtx, rty)];
    [self makeLineLayer:rootLayer lineFromPointA:CGPointMake(ltx, lty) toPointB:CGPointMake(lbx, lby)];
    [self makeLineLayer:rootLayer lineFromPointA:CGPointMake(lbx, lby) toPointB:CGPointMake(rbx, rby)];
    [self makeLineLayer:rootLayer lineFromPointA:CGPointMake(rtx, rty) toPointB:CGPointMake(rbx, rby)];
    
    UILabel *unablescan = [[UILabel alloc]initWithFrame:CGRectMake(0, frameWid / 5 + offset + frameWid + 50, rootLayer.bounds.size.width, 30)];
    unablescan.text = @"Unable to scan?";
    unablescan.textColor = [UIColor colorNamed:@"GrayTextColor"];
    unablescan.textAlignment = NSTextAlignmentCenter;
    [unablescan setFont: [unablescan.font fontWithSize: 15]];
    [self.view addSubview:unablescan];
    
    UILabel *description = [[UILabel alloc]initWithFrame:CGRectMake(0, frameWid / 5 + offset + frameWid + 80, rootLayer.bounds.size.width, 80)];
    description.text = @"In certain conditions (such as poor light) it may not\nbe possible to scan the serial number. If the serial\nnumber is not scanned automatically, tap the\nbutton below to enter it manually.";
    description.textColor = [UIColor colorNamed:@"GrayTextColor"];
    description.numberOfLines = 4;
    description.textAlignment = NSTextAlignmentCenter;
    [description setFont: [description.font fontWithSize: 12]];
    [self.view addSubview:description];
    
    // scan button add to buttonview
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    //button event
    [button addTarget:self
               action:@selector(manualInputClick:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Enter serial number" forState:UIControlStateNormal];
    //button coordinate
    button.frame = CGRectMake(40, frameWid / 5 + offset + frameWid + 170, rootLayer.bounds.size.width - 80, 45);
    button.layer.cornerRadius = 10;
    button.backgroundColor = [UIColor colorNamed:@"DarkerGrayTextColor"];
    [self.view addSubview:button];
    
    AVCaptureVideoDataOutput *output = [[AVCaptureVideoDataOutput alloc] init];
    [session addOutput:output];
    
    // Configure your output.
    dispatch_queue_t queue = dispatch_queue_create("myQueue", NULL);
    [output setSampleBufferDelegate:self queue:queue];
    
    // Specify the pixel format
    output.videoSettings =
    [NSDictionary dictionaryWithObject:
     [NSNumber numberWithInt:kCVPixelFormatType_32BGRA]
                                forKey:(id)kCVPixelBufferPixelFormatTypeKey];

    [session startRunning];
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    // Create a UIImage from the sample buffer data
    [connection setVideoOrientation:AVCaptureVideoOrientationPortrait];
    UIImage *image = [self imageFromSampleBuffer:sampleBuffer];
    
    float w = image.size.width;
    float h = image.size.height;
    
    CGRect rect = CGRectMake(w/6,h/9*4,w/3*2,h/9);
    
    // Create bitmap image from original image data,
    // using rectangle to specify desired crop area
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], rect);
    
    UIImage *img = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    UIImage* lineImg = [CVWrapper getBinaryImg:img];
    
    G8Tesseract *tesseract = [[G8Tesseract alloc] initWithLanguage:@"eng"];
    tesseract.delegate = self;
    tesseract.image = [lineImg g8_blackAndWhite];
    [tesseract recognize];
    
    [self getLineStr:[tesseract recognizedText]];
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

-(NSUInteger)navigationControllerSupportedInterfaceOrientations:(UINavigationController *)navigationController {
    return UIInterfaceOrientationMaskPortrait;
}


-(void)makeLineLayer:(CALayer *)layer lineFromPointA:(CGPoint)pointA toPointB:(CGPoint)pointB
{
    CAShapeLayer *line = [CAShapeLayer layer];
    UIBezierPath *linePath=[UIBezierPath bezierPath];
    [linePath moveToPoint: pointA];
    [linePath addLineToPoint:pointB];
    line.path=linePath.CGPath;
    line.fillColor = nil;
    line.lineWidth = 2.0;
    line.opacity = 1.0;
    line.strokeColor = [UIColor yellowColor].CGColor;
    [[self.view layer] addSublayer:line];
}

- (UIImage *) imageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer
{
    // Get a CMSampleBuffer's Core Video image buffer for the media data
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    // Lock the base address of the pixel buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    // Get the number of bytes per row for the pixel buffer
    void *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer);
    
    // Get the number of bytes per row for the pixel buffer
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    // Get the pixel buffer width and height
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    // Create a device-dependent RGB color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // Create a bitmap graphics context with the sample buffer data
    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8,
                                                 bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    // Create a Quartz image from the pixel data in the bitmap graphics context
    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
    // Unlock the pixel buffer
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    
    // Free up the context and color space
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    // Create an image object from the Quartz image
    UIImage *image = [UIImage imageWithCGImage:quartzImage];
    
    // Release the Quartz image
    CGImageRelease(quartzImage);
    
    return (image);
}

-(void)getLineStr:(NSString *)input {
    if(_alertFlag)
        return;
    NSArray* lines = [[input stringByReplacingOccurrencesOfString:@" " withString:@""] componentsSeparatedByString: @"\n"];
    
    //NSLog(@"%@",input);
    for(int i = 0; i < lines.count ; i++){
        NSArray* pieces = [[lines objectAtIndex: i] componentsSeparatedByString: @"-"];
        
        if( pieces.count == 2 ){
            NSCharacterSet *charactersToRemove = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
            NSString *temp1 = [[[pieces objectAtIndex:0] componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""];
            NSLog(@"temp1: %@",temp1);

            NSString *temp2 = [[[pieces objectAtIndex:1] componentsSeparatedByCharactersInSet:
                                [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                               componentsJoinedByString:@""];
            NSLog(@"temp2: %@",temp2);
            
            if([temp2 length] == 5){
                if([temp1 length] == 2){
//                    unichar char1 = [temp1 characterAtIndex:0];
//                    unichar char2 = [temp1 characterAtIndex:1];
                    _result = [NSString stringWithFormat:@"%@-%@", temp1, temp2];
//                    if (char1 >= 'A' && char1 <= 'Z' && char2 >= '0' && char2 <= '9') {
//
//                    }
                }
                else if([temp1 length] == 3){
                    unichar char1 = [temp1 characterAtIndex:0];
                    unichar char2 = [temp1 characterAtIndex:1];
                    unichar char3 = [temp1 characterAtIndex:2];
                    if (char1 >= 'A' && char1 <= 'Z' && char2 >= 'A' && char2 <= 'Z' && char3 >= '0' && char3 <= '9') {
                        _result = [NSString stringWithFormat:@"%@-%@", temp1, temp2];
                        
                    }
                }
            }

            if([_result length] != 0) {
                self->_alertFlag = YES;
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Serial Number" message:self->_result preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* okButton = [UIAlertAction
                                                actionWithTitle:@"OK"
                                                style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                                                {
                                                    [self DisplayResult:self->_result];
                                                }];
                    
                    UIAlertAction* rescanButton = [UIAlertAction
                                               actionWithTitle:@"Rescan"
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                                               {
                                                   self->_alertFlag = false;
                                                   self->_result = @"";
                                                   [self->session startRunning];
                                               }];
                    
                    [alert addAction:rescanButton];
                    [alert addAction:okButton];
                    [self presentViewController:alert animated:YES completion:nil];
                    [self->session stopRunning];
                });
            }
        }
    }
}

- (void)DisplayResult:(NSString*)input
{
    Boolean existflag = false;
    NSString* date;
    for (int i = 0; i < self.dataArray.count; i++){
        NSManagedObject *data = [self.dataArray objectAtIndex:i];
        NSString* db_num = [data valueForKey:@"serial_num"];
        if([input isEqualToString:db_num]){
            existflag = true;
            date = [data valueForKey:@"inspect_date"];
        }
    }
    
    if(existflag){
        NSString* message = [NSString stringWithFormat:@"%@%@", @"This device is already registered and was last serviced on ", date];
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Scan Successful" message:message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* detailButton = [UIAlertAction
                                   actionWithTitle:@"Details"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                       ExtinguishersController *controler = [storyboard instantiateViewControllerWithIdentifier:@"ExtinguishersController"];
                                       controler.input_email = self->_email;
                                       controler.input_phone = self.phone;
                                       controler.input_companyName = self.company;
                                       controler.input_personName = self.person;
                                       
                                       controler.purchase_date = self->_purchase_date;
                                       [self.navigationController pushViewController:controler animated:YES];
                                   }];
        
        UIAlertAction* cancelButton = [UIAlertAction
                                       actionWithTitle:@"Cancel"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           self->_alertFlag = false;
                                           self->_result = @"";
                                           [self->session startRunning];
                                       }];
        
        [alert addAction:cancelButton];
        [alert addAction:detailButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        NSString* message = @"The device has not been registered. Would you like to register it now?";
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"New Device" message:message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* registerButton = [UIAlertAction
                                       actionWithTitle:@"Register"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                           RegisterController *controler = [storyboard instantiateViewControllerWithIdentifier:@"RegisterController"];
                                           controler.result = self.result;
                                           controler.email = self.email;
                                           controler.phone = self.phone;
                                           controler.company = self.company;
                                           controler.person = self.person;
                                           controler.purchase_date = self.purchase_date;
                                           [self.navigationController pushViewController:controler animated:YES];
                                       }];
        
        UIAlertAction* cancelButton = [UIAlertAction
                                       actionWithTitle:@"Cancel"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           self->_alertFlag = false;
                                           [self->session startRunning];
                                       }];
        
        [alert addAction:cancelButton];
        [alert addAction:registerButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(IBAction)cancelClick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)manualInputClick:(id)sender{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Enter Serial Number" message:@"Please enter the serial number exactly as it appears on the extinguisher." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         self.alertFlag = true;
                                         self.result = alert.textFields.firstObject.text;
                                         [self DisplayResult:self.result];
                                     }];
    
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                   }];
    
    [alert addAction:cancelButton];
    [alert addAction:okButton];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField){
        textField.placeholder = @"Serial Number";
    }];
    [self presentViewController:alert animated:YES completion:nil];
}
@end
