//
//  InspectionChecklistController.m
//  MobileOCR
//
//  Created by CodingMaster on 12/12/18.
//  Copyright © 2018 CodingMaster. All rights reserved.
//

#import "InspectionChecklistController.h"
#import "ExtinguisherDetailController.h"
#import "VideoController.h"

@interface InspectionChecklistController ()
{
    UIButton *button;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
- (IBAction)pageControlDidPage:(id)sender;

@end

@implementation InspectionChecklistController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UINavigationBar* navbar = self.navigationController.navigationBar;
    navbar.tintColor = [UIColor colorNamed:@"GrayTextColor"];
    self.title = _serial_title;
    
    UIBarButtonItem *backBtn = [[UIBarButtonItem alloc]
                                initWithTitle:@"Back"
                                style:UIBarButtonItemStylePlain
                                target:self
                                action:@selector(backClick:)];
    self.navigationItem.leftBarButtonItem = backBtn;
    
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    
    self.scrollView.delegate = self;
    self.pageControl.numberOfPages = 8;
    self.pageControl.transform = CGAffineTransformMakeScale(1.4, 1.4);
}

-(void)viewWillAppear:(BOOL)animated {
    for(UIView *v in self.scrollView.subviews) {
        [v removeFromSuperview];
    }
}

-(void)viewDidAppear:(BOOL)animated {
    CGFloat pageWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat pageHeight = self.scrollView.bounds.size.height;
    
    self.scrollView.contentSize = CGSizeMake(8*pageWidth, 1);
    
    UIImageView *view1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, pageWidth, pageHeight)];
    view1.image = [UIImage imageNamed:@"Inspection1"];
    view1.contentMode = UIViewContentModeScaleAspectFit;
    
    UIImageView *view2 = [[UIImageView alloc] initWithFrame:CGRectMake(pageWidth, 0, pageWidth, pageHeight)];
    view2.image = [UIImage imageNamed:@"Inspection2"];
    view2.contentMode = UIViewContentModeScaleAspectFit;
    
    UIImageView *view3 = [[UIImageView alloc] initWithFrame:CGRectMake(2*pageWidth, 0, pageWidth, pageHeight)];
    view3.image = [UIImage imageNamed:@"Inspection3"];
    view3.contentMode = UIViewContentModeScaleAspectFit;
    
    UIImageView *view4 = [[UIImageView alloc] initWithFrame:CGRectMake(3*pageWidth, 0, pageWidth, pageHeight)];
    view4.image = [UIImage imageNamed:@"Inspection4"];
    view4.contentMode = UIViewContentModeScaleAspectFit;
    
    UIImageView *view5 = [[UIImageView alloc] initWithFrame:CGRectMake(4*pageWidth, 0, pageWidth, pageHeight)];
    view5.image = [UIImage imageNamed:@"Inspection5"];
    view5.contentMode = UIViewContentModeScaleAspectFit;
    
    UIImageView *view6 = [[UIImageView alloc] initWithFrame:CGRectMake(5*pageWidth, 0, pageWidth, pageHeight)];
    view6.image = [UIImage imageNamed:@"Inspection6"];
    view6.contentMode = UIViewContentModeScaleAspectFit;
    
    UIImageView *view7 = [[UIImageView alloc] initWithFrame:CGRectMake(6*pageWidth, 0, pageWidth, pageHeight)];
    view7.image = [UIImage imageNamed:@"Inspection7"];
    view7.contentMode = UIViewContentModeScaleAspectFit;
    
    UIView *view8 = [[UIImageView alloc] initWithFrame:CGRectMake(7*pageWidth, 0, pageWidth, pageHeight)];
    UILabel *complete_title = [[UILabel alloc] initWithFrame:CGRectMake(0, pageHeight*0.05, pageWidth, 30)];
    complete_title.text = @"Inspection Complete";
    complete_title.font = [UIFont boldSystemFontOfSize:13];
    complete_title.textColor = [UIColor darkGrayColor];
    complete_title.textAlignment = NSTextAlignmentCenter;
    
    UILabel *complete_label = [[UILabel alloc] initWithFrame:CGRectMake(0, pageHeight*0.1, pageWidth, 100)];
    complete_label.text = @"Tap the image below to save a record\n of this inspection and return to\n the device overview";
    complete_label.font = [UIFont systemFontOfSize:15];
    complete_label.numberOfLines = 5;
    complete_label.textColor = [UIColor lightGrayColor];
    complete_label.textAlignment = NSTextAlignmentCenter;
    
    button = [[UIButton alloc] init];
    button.userInteractionEnabled = YES;
    //button event
    [button addTarget:self action:@selector(startClick:) forControlEvents:UIControlEventTouchUpInside];
    view8.userInteractionEnabled = YES;
    
    UIImage *img = [UIImage imageNamed:@"tick1"];
    [button setBackgroundImage:img forState:UIControlStateNormal];
    //button coordinate
    button.frame = CGRectMake(pageWidth / 2 - 80, pageHeight*0.1+100, 160, 160);
    
    [view8 addSubview:complete_title];
    [view8 addSubview:complete_label];
    [view8 addSubview:button];
    
    [self.scrollView addSubview:view1];
    [self.scrollView addSubview:view2];
    [self.scrollView addSubview:view3];
    [self.scrollView addSubview:view4];
    [self.scrollView addSubview:view5];
    [self.scrollView addSubview:view6];
    [self.scrollView addSubview:view7];
    [self.scrollView addSubview:view8];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    self.pageControl.currentPage = (NSInteger)self.scrollView.contentOffset.x/self.scrollView.bounds.size.width;
    CGFloat xOffset = [UIScreen mainScreen].bounds.size.width * (CGFloat)self.pageControl.currentPage;
    [self.scrollView setContentOffset:CGPointMake(xOffset, 0) animated:YES];
}

- (IBAction)pageControlDidPage:(id)sender {
    CGFloat xOffset = [UIScreen mainScreen].bounds.size.width * (CGFloat)self.pageControl.currentPage;
    [self.scrollView setContentOffset:CGPointMake(xOffset, 0) animated:YES];
}

-(IBAction)backClick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)startClick:(id)sender{
    UIImage *img = [UIImage imageNamed:@"tick"];
    [button setBackgroundImage:img forState:UIControlStateNormal];
    
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self.navigationController popViewControllerAnimated:YES];
    });
}

- (IBAction)videoClick:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VideoController* video = [storyboard instantiateViewControllerWithIdentifier:@"VideoController"];
    [self.navigationController pushViewController:video animated:YES];
}

@end
