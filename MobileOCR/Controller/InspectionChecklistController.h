//
//  InspectionChecklistController.h
//  MobileOCR
//
//  Created by CodingMaster on 12/12/18.
//  Copyright © 2018 CodingMaster. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface InspectionChecklistController : UIViewController <UIScrollViewDelegate>

@property NSString* serial_title;

@end

