//
//  VideoController.m
//  MobileOCR
//
//  Created by CodingMaster on 1/31/19.
//  Copyright © 2019 CodingMaster. All rights reserved.
//

#import "VideoController.h"

@interface VideoController ()
{
    UIWebView* videoView;
}

@end

@implementation VideoController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UINavigationBar* navbar = self.navigationController.navigationBar;
    navbar.tintColor = [UIColor colorNamed:@"GrayTextColor"];
    self.title = @"Video";
    
    UIBarButtonItem *backBtn = [[UIBarButtonItem alloc]
                                initWithTitle:@"Back"
                                style:UIBarButtonItemStylePlain
                                target:self
                                action:@selector(backClick:)];
    self.navigationItem.leftBarButtonItem = backBtn;
    
    NSString *videoURL = @"http://www.youtube.com/embed/F3VBfgSA5EQ";
    int width = self.view.bounds.size.width;
    int height = self.view.bounds.size.height;
    videoView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    videoView.backgroundColor = [UIColor clearColor];
    videoView.opaque = NO;
    videoView.delegate = self;
    [self.view addSubview:videoView];
    
    
    NSString *videoHTML = [NSString stringWithFormat:@"\
                           <html>\
                           <head>\
                           <style type=\"text/css\">\
                           iframe {position:absolute; top:50%%; margin-top:-130px;}\
                           body {background-color:#000; margin:0;}\
                           </style>\
                           </head>\
                           <body>\
                           <iframe width=\"100%%\" height=\"240px\" src=\"%@\" frameborder=\"0\" allowfullscreen></iframe>\
                           </body>\
                           </html>", videoURL];
    
    [videoView loadHTMLString:videoHTML baseURL:nil];
    [self.view addSubview:videoView];
}

-(IBAction)backClick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
