//
//  EmailAddressController.h
//  MobileOCR
//
//  Created by CodingMaster on 12/1/18.
//  Copyright © 2018 CodingMaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <FirebaseAuth.h>

NS_ASSUME_NONNULL_BEGIN

@interface EmailAddressController : UIViewController <UITextFieldDelegate>
{
    float animatedDistance;
    NSString *strEmail;
    NSString *strPhone;
    NSString *strCompanyName;
    NSString *strPersonalName;
    FIRAuthStateDidChangeListenerHandle handeler;
}
@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (strong,nonatomic)  NSMutableArray *dataArray;
@end

NS_ASSUME_NONNULL_END
