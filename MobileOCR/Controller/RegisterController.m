//
//  RegisterController.m
//  MobileOCR
//
//  Created by CodingMaster on 12/12/18.
//  Copyright © 2018 CodingMaster. All rights reserved.
//

#import "RegisterController.h"
#import "ScanController.h"
#import <MessageUI/MessageUI.h>

@interface RegisterController ()<MFMailComposeViewControllerDelegate>{
    NSString *newDateString;
}

@property (weak, nonatomic) IBOutlet UILabel *serialNum_Label;
@property (weak, nonatomic) IBOutlet UILabel *purchase_label;
@property (weak, nonatomic) IBOutlet UITextField *location_input;

@end

@implementation RegisterController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.location_input.delegate = self;
    _location_input.returnKeyType = UIReturnKeyDone;
    _location_input.autocapitalizationType = UITextAutocapitalizationTypeWords;
    //navBar set
    UINavigationBar* navbar = self.navigationController.navigationBar;
    navbar.tintColor = [UIColor colorNamed:@"GrayTextColor"];
    self.title = @"Register device";
    [navbar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorNamed:@"RedTextColor"]}];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]
                                  initWithTitle:@"Cancel"
                                  style:UIBarButtonItemStylePlain
                                  target:self
                                  action:@selector(cancelClick:)];
    self.navigationItem.rightBarButtonItem = cancelBtn;
    self.navigationItem.hidesBackButton = YES;
    
    NSDate * now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd-MM-yyyy"];
    newDateString = [outputFormatter stringFromDate:now];
    
    _serialNum_Label.text = _result;
    _purchase_label.text = newDateString;
    
}

-(IBAction)cancelClick:(id)sender{
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    ScanController *registdev = [storyboard instantiateViewControllerWithIdentifier:@"ScanController"];
//    registdev.email = _email;
//    registdev.purchase_date = _purchase_date;
//    [self.navigationController pushViewController:registdev animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)registerClick:(id)sender {
    if([newDateString isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                        message:@"Please input Location Info."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else{
        
        NSLog(@"email ,%@",_email);
        
        NSString *strBody = [NSString stringWithFormat:@"Email: %@ \nSerial no: %@ \nCompany Name: %@ \nPerson name: %@ \nTel no: %@",_email,_result,_company,_person,_phone];
        
        if([MFMailComposeViewController canSendMail]) {
            MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
            mailCont.mailComposeDelegate = self;
            
            [mailCont setSubject:@"New Warranty"];
            [mailCont setToRecipients:[NSArray arrayWithObject:@"sales@britannia-fire.co.uk"]];
            [mailCont setCcRecipients:[NSArray arrayWithObject:_email]];
            [mailCont setMessageBody:strBody isHTML:NO];
            [self presentViewController:mailCont animated:YES completion:nil];
        }else
        {
//            print("This device is not configured to send email. Please set up an email account.")
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                            message:@"This device is not configured to send email. Please set up an email account."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
    
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    WarrantyController *warranty = [storyboard instantiateViewControllerWithIdentifier:@"WarrantyController"];
    warranty.email = _email;
    warranty.phone = _phone;
    warranty.company = _company;
    warranty.person = _person;
    warranty.purchase_date = _purchase_date;
    warranty.dateString = newDateString;
    warranty.result = _result;
    warranty.location = _location_input.text;
    
    [self.navigationController pushViewController:warranty animated:YES];
    
}
@end

