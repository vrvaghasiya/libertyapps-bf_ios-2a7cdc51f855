//
//  WarrantyController.h
//  MobileOCR
//
//  Created by CodingMaster on 12/12/18.
//  Copyright © 2018 CodingMaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FirebaseDatabase/FirebaseDatabase.h>

@interface WarrantyController : UIViewController
{
    long long timestamp;
}
@property NSString* result;
@property NSString* email;
@property NSString* phone;
@property NSString* company;
@property NSString* person;
@property NSString* purchase_date;
@property NSString* dateString;
@property NSString* location;

@property (strong, nonatomic) FIRDatabaseReference *ref;

@end

