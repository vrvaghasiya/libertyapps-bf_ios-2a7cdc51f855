//
//  ExtinguisherDetailController.h
//  MobileOCR
//
//  Created by CodingMaster on 12/12/18.
//  Copyright © 2018 CodingMaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <FirebaseDatabase/FirebaseDatabase.h>
#import <FirebaseAuth.h>
@interface ExtinguisherDetailController : UIViewController


@property NSString* phone;
@property NSString* company;
@property NSString* person;
@property (strong, nonatomic) FIRDatabaseReference *ref;

@property NSString* purchase_date;
@property NSManagedObject *data;
@property NSDictionary *dataDic;
@property BOOL isAdmin;


@end

