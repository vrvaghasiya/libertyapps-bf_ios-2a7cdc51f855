//
//  ResultController.h
//  MobileOCR
//
//  Created by CodingMaster on 11/14/18.
//  Copyright © 2018 CodingMaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "ScanController.h"


@interface ResultController : UIViewController<UITextFieldDelegate>

@property NSString* resultstr;
@property UIViewController *tempVC;

@end
