//
//  ExtinguishersController.m
//  MobileOCR
//
//  Created by CodingMaster on 12/2/18.
//  Copyright © 2018 CodingMaster. All rights reserved.
//

#import "ExtinguishersController.h"
#import "ExtinguisherDetailController.h"
#import "ScanController.h"
#import "AppDelegate.h"
#import "AccountController.h"
#import <UserNotifications/UserNotifications.h>

@interface ExtinguishersController ()<UITableViewDelegate,UITableViewDataSource,UNUserNotificationCenterDelegate>

@property (strong,nonatomic) UITableView *table;

@end

@implementation ExtinguishersController

- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if ([delegate respondsToSelector:@selector(persistentContainer)]) {
        context = delegate.persistentContainer.viewContext;
    }
    return context;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UINavigationBar* navbar = self.navigationController.navigationBar;
    navbar.tintColor = [UIColor colorNamed:@"GrayTextColor"];
    self.title = @"Extinguishers";
    [navbar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorNamed:@"RedTextColor"]}];
    
    UIBarButtonItem *scanBtn = [[UIBarButtonItem alloc]
                                  initWithTitle:@"Scan"
                                  style:UIBarButtonItemStylePlain
                                  target:self
                                  action:@selector(scanClick:)];
    self.navigationItem.rightBarButtonItem = scanBtn;
    
    UIBarButtonItem *accountBtn = [[UIBarButtonItem alloc]
                                initWithTitle:@"Account"
                                style:UIBarButtonItemStylePlain
                                target:self
                                action:@selector(accountClick:)];
    self.navigationItem.leftBarButtonItem = accountBtn;
    self.navigationController.navigationBarHidden = false;
    [self cofigureTableview];
    
    [self getData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
           selector:@selector(receiveTestNotification:)
           name:@"ReloadData"
           object:nil];
}
-(void)getData{
    // Fetch the devices from persistent data store
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Data"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"email = %@",_input_email];
    [fetchRequest setPredicate:predicate];
    
    self.dataArray = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    int OldRecord = 0;
    for (NSManagedObject *data in self.dataArray) {
        NSString *strDetail = [NSString stringWithFormat:@"serialNo:%@  Location:%@",[data valueForKey:@"serial_num"] ,[data valueForKey:@"location"] ];
      
        long long timestamp = [[data valueForKey:@"timestamp"] longLongValue];
        if (timestamp > 0 ) {
            [self setLocalNotification:[data valueForKey:@"inspect_date"] :[data valueForKey:@"timestamp"] :strDetail];
        }else{
            long long current_timestamp = [NSDate date].timeIntervalSince1970;
            [self setLocalNotification:[data valueForKey:@"inspect_date"] :[NSString stringWithFormat:@"%lli",current_timestamp + OldRecord] :strDetail];
            OldRecord += 1;
        }
  }
    NSLog(@"phone: %@",_input_phone);
    NSLog(@"company: %@",_input_companyName);
    NSLog(@"Person: %@",_input_personName);
    [self.table reloadData];
}
- (void) receiveTestNotification:(NSNotification *) notification
{
    [self getData];
}
-(void)cofigureTableview
{
    self.table = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.table.delegate = self;
    self.table.dataSource = self;
    [self.view addSubview:self.table];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    
    UITableViewCell *cell = [self.table dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    NSManagedObject *data = [self.dataArray objectAtIndex:indexPath.row];
    
    CGFloat wid = cell.bounds.size.width;
    CGFloat selfHeight = self.view.bounds.size.height / 9;
    //Initialize Label
    UILabel *lbl1 = [[UILabel alloc]initWithFrame:CGRectMake(20,
                                                             0,
                                                             wid - 20,
                                                             selfHeight / 2)];
    [lbl1 setFont:[UIFont systemFontOfSize:20.0]];
    [lbl1 setTextColor:[UIColor colorNamed:@"DarkGrayTextColor"]];
    lbl1.text = [data valueForKey:@"serial_num"];
    [cell addSubview:lbl1];
    
    UILabel *lbl2 = [[UILabel alloc]initWithFrame:CGRectMake(20 ,
                                                             selfHeight / 2,
                                                             wid - 20,
                                                             selfHeight / 2)];
    [lbl2 setFont:[UIFont systemFontOfSize:14.0]];
    [lbl2 setTextColor:[UIColor colorNamed:@"GrayTextColor"]];
    lbl2.text = [@"Last Inspected: " stringByAppendingString:[data valueForKey:@"inspect_date"]];
    [cell addSubview:lbl2];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat selfHeight=self.view.bounds.size.height / 10;
    return selfHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    //NSLog(@"title of cell %@", [_content objectAtIndex:indexPath.row]);
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ExtinguisherDetailController *extinguishers = [storyboard instantiateViewControllerWithIdentifier:@"ExtinguisherDetailController"];
    extinguishers.purchase_date = _purchase_date;
    extinguishers.phone = _input_phone;
    extinguishers.company = _input_companyName;
    extinguishers.person = _input_personName;
    extinguishers.data = [self.dataArray objectAtIndex:indexPath.row];
    extinguishers.isAdmin = false;
    [self.navigationController pushViewController:extinguishers animated:YES];
}

-(IBAction)scanClick:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ScanController *scanner = [storyboard instantiateViewControllerWithIdentifier:@"ScanController"];
    scanner.email = _input_email;
    scanner.phone = _input_phone;
    scanner.company = _input_companyName;
    scanner.person = _input_personName;
    scanner.purchase_date = _purchase_date;
    [self.navigationController pushViewController:scanner animated:YES];
}

-(IBAction)accountClick:(id)sender{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AccountController* account = [storyboard instantiateViewControllerWithIdentifier:@"AccountController"];
    account.email = _input_email;
    account.phone = _input_phone;
    account.company = _input_companyName;
    account.person = _input_personName;
    [self.navigationController pushViewController:account animated:YES];
}
-(NSArray*) getNextInspect:(NSString*) input
{
    NSArray* listItems = [input componentsSeparatedByString:@"-"];
    NSString* year = [listItems objectAtIndex:2];
    NSString* month = [listItems objectAtIndex:1];
    NSString* day = [listItems objectAtIndex:0];
    return [NSArray arrayWithObjects:year,month,day,nil];
}
-(void)setLocalNotification:(NSString *)strdate :(NSString *)timestamp :(NSString *)detail{
    UNUserNotificationCenter *center  = [UNUserNotificationCenter currentNotificationCenter];
    UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc]init];
    content.title = @"Service Reminder";
    content.subtitle = @"Check on list..";
    content.body = detail;
    content.sound = UNNotificationSound.defaultSound;
    
    NSDateComponents* date1 = [[NSDateComponents alloc] init];
    NSArray *dateTime = [self getNextInspect:strdate];
    date1.year = [[dateTime objectAtIndex:0] intValue] + 1; // Notification come after 1 year..
    date1.month = [[dateTime objectAtIndex:1] intValue];
    date1.day = [[dateTime objectAtIndex:2] intValue];
    date1.hour = 16;
    date1.minute = 55;
    
    UNCalendarNotificationTrigger* trigger = [UNCalendarNotificationTrigger
    triggerWithDateMatchingComponents:date1 repeats:YES];
        
    center.delegate = self;
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:timestamp content:content trigger:trigger];
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        NSLog(@"error %@",error.description);
    }];
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    
}
@end
