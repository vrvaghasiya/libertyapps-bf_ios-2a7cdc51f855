//
//  ExtinguisherDetailController.m
//  MobileOCR
//
//  Created by CodingMaster on 12/12/18.
//  Copyright © 2018 CodingMaster. All rights reserved.
//

#import "ExtinguisherDetailController.h"
#import "ExtinguishersController.h"
#import "InspectionChecklistController.h"
#import "WarrantyShowController.h"
#import "AppDelegate.h"
#import <Firebase.h>
#import <FIRAuth.h>

@interface ExtinguisherDetailController ()
{
    UIAlertView * alert;
    UITextField * alertTextField;
}

@property (weak, nonatomic) IBOutlet UILabel *purchasedateLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastinspectdateLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *nextInspectionDate;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;

@end

@implementation ExtinguisherDetailController
@synthesize isAdmin;
- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if ([delegate respondsToSelector:@selector(persistentContainer)]) {
        context = delegate.persistentContainer.viewContext;
    }
    return context;
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    UINavigationBar* navbar = self.navigationController.navigationBar;
    navbar.tintColor = [UIColor colorNamed:@"GrayTextColor"];
    
    if (isAdmin == true) {
        self.title = [_dataDic valueForKey:@"serial_num"];
        self.purchasedateLabel.text = _purchase_date;
        self.lastinspectdateLabel.text = [_dataDic valueForKey:@"inspect_date"];
        self.emailLabel.text = [_dataDic valueForKey:@"email"];
        self.locationLabel.text = [_dataDic valueForKey:@"location"];
        self.nextInspectionDate.text = [self getNextInspect:[_dataDic valueForKey:@"inspect_date"]];
        [self.btnDelete setHidden:true];
        
    }else{
        
        [self.btnDelete setHidden:false];
        self.title = [_data valueForKey:@"serial_num"];
        self.purchasedateLabel.text = _purchase_date;
        self.lastinspectdateLabel.text = [_data valueForKey:@"inspect_date"];
        self.emailLabel.text = [_data valueForKey:@"email"];
        self.locationLabel.text = [_data valueForKey:@"location"];
        self.nextInspectionDate.text = [self getNextInspect:[_data valueForKey:@"inspect_date"]];
        
        [navbar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor colorNamed:@"RedTextColor"]}];
        
        UIBarButtonItem *moveBtn = [[UIBarButtonItem alloc]
                                    initWithTitle:@"Move"
                                    style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(moveClick:)];
        self.navigationItem.rightBarButtonItem = moveBtn;
    }
    
    UIBarButtonItem *backBtn = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Back"
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(backClick:)];
    self.navigationItem.leftBarButtonItem = backBtn;
}

-(IBAction)moveClick:(id)sender{
    alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter new location!" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    alertTextField = [alert textFieldAtIndex:0];
    //alertTextField.keyboardType = UIKeyboardTypeNumberPad;
    //alertTextField.returnKeyType = UIReturnKeyDone;
    alertTextField.placeholder = @"Location";
    [alert show];
}

-(IBAction)backClick:(id)sender{
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    ExtinguishersController *extinguishers = [storyboard instantiateViewControllerWithIdentifier:@"ExtinguishersController"];
//    extinguishers.input_email = [_data valueForKey:@"email"];
//    extinguishers.purchase_date = _purchase_date;
//    extinguishers.input_phone = _phone;
//    extinguishers.input_companyName = _company;
//    extinguishers.input_personName = _phone;
//    [self.navigationController pushViewController:extinguishers animated:YES];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)inspectnowBtn:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    InspectionChecklistController *inspection = [storyboard instantiateViewControllerWithIdentifier:@"InspectionChecklistController"];
    inspection.serial_title = [_data valueForKey:@"serial_num"];
    [self.navigationController pushViewController:inspection animated:YES];
}

- (IBAction)viewwarrantyBtn:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    WarrantyShowController *warranty = [storyboard instantiateViewControllerWithIdentifier:@"WarrantyShowController"];
    [self.navigationController pushViewController:warranty animated:YES];
}
- (IBAction)btnDelete:(id)sender {
    
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Are you sure to delete this record?"
                                                                   message:nil
                                   preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction* yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault
       handler:^(UIAlertAction * action) {
        
        
        // Delete record from Firebase
        self.ref = [[FIRDatabase database] reference];
        if ([FIRAuth auth].currentUser.uid != nil) {
            NSString *strID = [NSString stringWithFormat:@"%lld",[[self->_data valueForKey:@"timestamp"] longLongValue]];
            [[[[self.ref child:@"Data"] child:[FIRAuth auth].currentUser.uid] child:[NSString stringWithFormat:@"%@",strID]] removeValue];
        }
        
        // Delete record also from local database (CoreData)
        [self deleteRecord];
        
        // Back to list screen..
        [self.navigationController popViewControllerAnimated:true];
        
    }];
    UIAlertAction* notAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault
       handler:^(UIAlertAction * action) {}];

    [alert addAction:yesAction];
    [alert addAction:notAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)deleteRecord{
    NSString *timeStamp = [NSString stringWithFormat:@"%lld",[[_data valueForKey:@"timestamp"] longLongValue]];

    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Data" inManagedObjectContext:context];
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"email = %@",[_data valueForKey:@"email"]];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"timestamp = %@",timeStamp];
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
    
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];

    NSError *error;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];

    for (NSManagedObject *managedObject in items)
    {
        [context deleteObject:managedObject];
    }
}


-(NSString*) getNextInspect:(NSString*) input
{
    NSArray* listItems = [input componentsSeparatedByString:@"-"];
    NSString* year = [listItems objectAtIndex:2];
    NSString* month = [listItems objectAtIndex:1];
    NSString* day = [listItems objectAtIndex:0];
    
    int yeardate = [year intValue] + 1;
    NSString* result = [NSString stringWithFormat:@"%@-%@-%d",day,month,yeardate];
    
    return result;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0)
    {
        [alert dismissWithClickedButtonIndex:-1 animated:YES];
    }
    if (buttonIndex == 1)
    {
        NSString* location = alertTextField.text;
        if([location isEqualToString:@""]){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                            message:@"Please input location!"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        else{
            self.locationLabel.text = location;
            NSManagedObjectContext *context = [self managedObjectContext];
            [self.data setValue:location forKey:@"location"];
            
            NSError *error = nil;
            // Save the object to persistent store
            if (![context save:&error]) {
                NSLog(@"Can't Update! %@ %@", error, [error localizedDescription]);
            }
        }

    }
}

@end
