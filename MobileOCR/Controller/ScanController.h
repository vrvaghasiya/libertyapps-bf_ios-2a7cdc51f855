//
//  ViewController.h
//  MobileOCR
//
//  Created by CodingMaster on 11/7/18.
//  Copyright © 2018 CodingMaster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TesseractOCR/TesseractOCR.h>
#import <AVFoundation/AVCaptureSession.h>
#import <AVFoundation/AVCaptureStillImageOutput.h>
#import <AVFoundation/AVCaptureInput.h>
#import <AVFoundation/AVCaptureVideoPreviewLayer.h>
#import <AVFoundation/AVCaptureVideoDataOutput.h>
#include "CVWrapper.h"

@interface ScanController : UIViewController<G8TesseractDelegate,UIAlertViewDelegate>

@property BOOL alertFlag;
@property NSString* result;
@property NSString* email;
@property NSString* phone;
@property NSString* company;
@property NSString* person;
@property NSString* purchase_date;
@property (strong) NSMutableArray *dataArray;

@end

