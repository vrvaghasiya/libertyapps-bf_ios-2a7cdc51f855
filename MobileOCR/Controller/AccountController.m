//
//  AccountController.m
//  MobileOCR
//
//  Created by CodingMaster on 12/12/18.
//  Copyright © 2018 CodingMaster. All rights reserved.
//

#import "AccountController.h"
#import "EmailAddressController.h"
#import "AdminLoginVC.h"
#import <FIRAuth.h>
@interface AccountController ()

@property (weak, nonatomic) IBOutlet UILabel *emailName;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblCompanyName;
@property (weak, nonatomic) IBOutlet UILabel *lblPersonName;

@end

@implementation AccountController

- (void)viewDidLoad {
    [super viewDidLoad];

    _emailName.text = _email;
    _lblPhone.text = _phone;
    _lblCompanyName.text = _company;
    _lblPersonName.text = _person;
    
    
    UINavigationBar* navbar = self.navigationController.navigationBar;
    navbar.tintColor = [UIColor colorNamed:@"GrayTextColor"];
    self.title = @"Account";
    [navbar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorNamed:@"RedTextColor"]}];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc]
                                  initWithTitle:@"Cancel"
                                  style:UIBarButtonItemStylePlain
                                  target:self
                                  action:@selector(cancelClick:)];
    self.navigationItem.rightBarButtonItem = cancelBtn;
    self.navigationItem.hidesBackButton = YES;
}

- (IBAction)changeAddress:(id)sender {
    BOOL status = [[FIRAuth auth] signOut:nil];
    NSLog(@"status %i",status);
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    AdminLoginVC *VC = [storyboard instantiateViewControllerWithIdentifier:@"AdminLoginVC"];
    [self.navigationController popToRootViewControllerAnimated:YES];
//    [self.navigationController pushViewController:VC animated:YES];
}

-(IBAction)cancelClick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
